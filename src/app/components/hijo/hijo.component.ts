import { Component, OnInit } from '@angular/core';
import { Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-hijo',
  templateUrl: './hijo.component.html',
  styleUrls: ['./hijo.component.css']
})
export class HijoComponent implements OnInit {

  Persona ={
    nombre:'Juan Pablo',
    apellidos: 'Sanchez Gonzalez',
    edad: 23,
    sexo: 'Masculino',
    estado: 'Soltero',
    mensaje: 'Objeto del hijo al padre recibido'
  }

  Triangulo ={
    forma:'Triangulo',
    tipo:'Rectangulo',
    lados: ['Cateto Opuesto','Cateto Adyacente','Hipotenusa'],
    color: 'Amarillo',
    mensaje: 'Objeto-2 del hijo al padre recibido'
  }

  @Output() paraEmitir = new EventEmitter<any>();
  @Output() alPadre = new EventEmitter<any>();
  constructor() { }

  ngOnInit(): void {
    this.paraEmitir.emit(this.Persona);
    this.alPadre.emit(this.Triangulo)
  }

}
